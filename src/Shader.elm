module Shader
  exposing
    ( vertex
    , fragment
    )


import Math.Vector2 as Vec2
  exposing
    ( Vec2
    )
import WebGL


vertex : WebGL.Shader { position : Vec2 } {} { pos : Vec2 }
vertex =
  [glsl|
    varying vec2 pos;
    attribute vec2 position;

    void main () {
      gl_Position = vec4(position, 0., 1.);
      pos = position;
    }
  |]


fragment : WebGL.Shader {} {} { pos : Vec2 }
fragment =
  [glsl|
    precision mediump float;
    varying vec2 pos;

    vec3 poincare_to_hyperboloid(vec2 poincare) {
      float posDot = dot(poincare, poincare);
      return vec3(1. + posDot, 2.*poincare) / (1. - posDot);
    }

    void main () {
      float posDot = dot(pos, pos);
      if ( posDot >= 1. ) {
        gl_FragColor = vec4(0., 0., 0., 1.);
      } else {
        vec3 h1 = poincare_to_hyperboloid(vec2(0.3, 0.3));
        vec3 h2 = poincare_to_hyperboloid(vec2(0.7, 0.2));
        vec3 hp1 = cross(h1,h2);
        vec3 hyperboloid = poincare_to_hyperboloid(pos);
        gl_FragColor = vec4(vec3(smoothstep(0.0, 0.1, smoothstep(0.0, 0.1, smoothstep(0.0, 0.1,abs(dot(hyperboloid, hp1)))))), 1.);
      }
    }
  |]
