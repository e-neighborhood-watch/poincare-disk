module Main
  exposing
    ( main
    )


import Shader


import Browser
import Html.Attributes as HtmlAttr
import Math.Vector2 as Vec2
  exposing
    ( Vec2
    , vec2
    )
import WebGL


type alias Model =
  {}


type Message
  = Noop


init : () -> ( Model, Cmd Message )
init _ =
  ( {}
  , Cmd.none
  )


view : Model -> Browser.Document Message
view {} =
  { title =
    "Poincare Disk Model"
  , body =
    [ WebGL.toHtml
      [ HtmlAttr.width 500
      , HtmlAttr.height 500
      , HtmlAttr.style "display" "block"
      ]
      [ WebGL.entity
        Shader.vertex
        Shader.fragment
        ( WebGL.indexedTriangles
          [ { position =
              vec2 -1 -1
            }
          , { position =
              vec2 1 -1
            }
          , { position =
              vec2 1 1
            }
          , { position =
              vec2 -1 1
            }
          ]
          [ ( 0, 1, 2 )
          , ( 2, 3, 0 )
          ]
        )
        {}
      ]
    ]
  }


update : Message -> Model -> ( Model, Cmd Message )
update msg prev =
  case
    msg
  of
    Noop ->
      ( prev
      , Cmd.none
      )


subscriptions : Model -> Sub Message
subscriptions {} =
  Sub.none


main : Program () Model Message
main =
  Browser.document
    { init =
      init
    , view =
      view
    , update =
      update
    , subscriptions =
      subscriptions
    }
